#!/usr/bin/python3
import os
import sys

PYTHON='/usr/bin/python3'
PREFIX='%PREFIX%'

if __name__ == '__main__':
    self = os.path.basename(sys.argv[0])
    if self in ('midsu', 'chmid', 'midlaunch'):
        target = os.path.join(PREFIX, self) + '.py'
        
        os.execv(PYTHON,
            ('python3', '-E', target, *sys.argv[1:])
        )
