Three utils protecting your secrets from other programs.

* [The problem](#the-problem)
* [Shadow users](#shadow-users)
* [Midsu](#midsu)
* [Chmid](#chmid)
* [Midlaunch (requires testing)](#midlaunch-requires-testing)
* [Installation](#installation)
* [Security](#security)
    * [(In)secure startup](#in-secure-startup)
    * [Protecting critical user configs](#protecting-critical-user-configs)
* [Contributing](#contributing)

# The problem

Unix systems while being rock solid at multi-user security, they typically fail to provide users per-application access control, coming from outdated assumption that user trusts all the software they run. Thus, while unprivileged programs cannot install drivers, modify kernel parameters or manage user accounts, they have full access to your home directory, including: all your documents, SSH and GPG keys, web browsing history and cookies they can read and send over to the Internet. Or encrypt all your files and demand a ransom. Or make you unable to log in by appending to `~/.bashrc`.

![xkcd comics](https://imgs.xkcd.com/comics/authorization.png)

Throughout time, some solutions have arisen to the GNU/Linux land: containers isolating untrusted programs from the host, mandatory access control, firejail, bubblewrap, flatpak... and this is yet another workaround with slightly different approach: instead of isolating untrusted programs, it isolates sensitive files and let you decide, which programs can access them. This is achieved through utilizing Linux namespaces combined with a relatively new concept - *shadow users*.

# Shadow users

Shadow user is a user account linked to your primary user. You can store your sensitive files in its home directory (by default it's `~/.shadow-home`, e.g. `/home/anedroid/.shadow-home`). These are protected by unix file permissions - other unprivileged programs cannot access them unless running as shadow user.

A user can be either primary or shadow and there is 1:1 mapping between them.

# Midsu

usage: `midsu [--failsafe]`

arguments:
* `--failsafe` - Run default program ('/usr/bin/tmux', '/bin/bash', '/bin/sh')

This command logs you as shadow user to manage protected files and programs configuration. If shadow user does not exist, it attempts creating one. This behaviour can be disabled by adding following option to global config at `/etc/midsu`:

```ini
[main]
user_auto_create = false # <---

[shadow]
# shadow users configuration
```

With this option disabled, unprivileged users will not be able to auto-create shadow user accounts anymore.

When invoking `midsu`, a sudo password prompt might appear, but you don't need to be administrator. This is only intended to prevent gaining access by arbitrary programs.

You can customize your login program by adding executable file `.midsu` to the shadow user home directory. It can be a script, a binary or a symlink. For example:

```sh
ln -s /bin/zsh ~/.midsu
```

Make sure this file is not writable by anyone but your shadow user!

In case of broken login program, you can pass `--failsafe` argument, which result in running the default `/usr/bin/tmux` (or, if it's not installed, `/bin/bash` or `/bin/sh`).

# Chmid

usage: `chmid [-R] {upgrade|downgrade} [files ...]`

arguments:
* mode - Transfer direction
    * `upgrade` - from primary user to shadow user
    * `downgrade` - from shadow user to primary user
* `-R`, `--recursive` - Operate on directories recursively

This command switches file owner (*chown*) between primary and shadow user without administrator privileges.

For example:

```
# Upgrading file
[anedroid@localhost]:~ $ touch secret
[anedroid@localhost]:~ $ ls -l secret
-rw-r--r--.  1 anedroid anedroid 0 08-09 20:24 secret
[anedroid@localhost]:~ $ chmid upgrade secret
[anedroid@localhost]:~ $ ls -l secret
-rw-r--r--. 1 anedroid-shadow anedroid 0 08-09 20:24 secret

# Downgrading file
[anedroid@localhost]:~ $ midsu
[sudo] password for anedroid:
[anedroid-shadow@localhost]:/home/anedroid $ chmid downgrade secret
[anedroid-shadow@localhost]:/home/anedroid $ ls -l secret
-rw-r--r--. 1 anedroid anedroid 0 08-09 20:24 secret
```

Note that only the owner of the file can use `chmid` on it. If you try to touch file owned by someone else, you'll see an ugly warning message:

`Warning: file 'secret' is owned by someone else`

That said, upgrading is possible only as primary user and downgrading only as shadow user. So if you need to downgrade something, you need to "upgrade yourself" first with `midsu` command!

When the command is invoked on a directory, only the directory owner will be changed. If you need to operate recursively, use `-R` argument. Symlinks are going to be ignored.

Chmid will not work if sysctl `fs.protected_hardlinks` is disabled for security reasons.

# 🚧️ Midlaunch (requires testing)

usage: `midlaunch [-v] [program] [arguments ...]`

arguments:
* `-v`, `--verbose` - display pre-startup debug messages

The last, but most important utility (and the most complex) for running trusted programs with the defined set of directories it can read and write to that will be mounted in the new mounting namespace.

Midlaunch reads `.midlaunch` file from your shadow home and parses its content. If the desired program exists among defined containers, midlaunch creates a namespace, binds directories and finally run the program as primary user. For example:

```ini
# full path
[firefox]
exec = /usr/bin/firefox
mounts =
    /home/anedroid/.shadow-home/.mozilla -> /home/anedroid/.mozilla

# or shorter
[ssh]
exec = /usr/bin/ssh
mounts =
    .ssh -> .ssh

# or shorter even more
[gnupg]
exec = /usr/bin/gpg
mounts =
    .gnupg
```

The config syntax is similar to ini. Each section is a different program. Its name does not matter. Each section may have following properties:

* `exec` - path to executable file. Multiple newline-separated values are accepted. As in example above, values spread across multiple lines must be intended. Duplicate execs are not allowed.
* `extends` - name of the section that the current inherits from. In case of circular inheritance, an exception is raised.
* `mounts` - newline-separated directories to bind. If relative path is given, it's resolved against home directory - shadow's for source and primary's for target. Arrow `->` separates source and target. If arrow is missing, midlaunch assumes these are the same (so absolute paths without arrow are invalid, because they point to the same directory). Paths can be "quoted". You can mount only in primary homedir.
* `run_as_user` - controls whether the program is being running as shadow user. This should be used with extreme caution, because running program as shadow user effectively means granting it access to all secret files. The indented use-case is elevated terminal emulator without password prompt. Default is false.
* `permit_args` - controls whether passing additional command-line arguments is allowed. Default is true.

`mounts` behaves specially when extending - instead of overriding, it's merged with parent. Mounts of the same target get overrided. A user can remove parent mount in the child by specifying mount of empty source.

Midlaunch is almost finished, but not throughoutly tested how it behaves under different conditions. It can have a lot of bugs. At the moment flatpaks does not work with it. **If you know Python and Linux, your help would be really appreciated!**

Future updates might introduce new features like: encrypted vaults, network config (e.g. routing traffic through a VPN), per-app profiles (like web browser profiles but for any app), option to require authentication, etc.


# Installation

Make sure all these things are installed on your machine:

* Linux,
* python3,
* sudo,
* su,
* useradd,
* bash,
* make,
* bwrap,
* tmux (recommended for security reasons)

First, download this repository. Then run `make install` as root. Global config file (`/etc/midsu`) will be created automatically if it does not exist yet.

Test if everything works by entering `midsu`. It should ask for your password, create your shadow user and log you in.

To uninstall, run `make clean` in the project's directory. Uninstallation keeps all your files, users and configuration in-place.

Midutils stores files in following locations:

* `/usr/local/lib/midsu` (`prefix` variable in Makefile),
* Symlinks `midsu`, `chmid` and `midlaunch` in `/usr/local/bin` linking to `/usr/local/lib/midsu/launcher.py`,
* Sudoers configuration in `/etc/sudoers/99-midsu`,
* Global configuration in `/etc/midsu`,
* Shadow user homedirs in `~/.shadow-home`,
* Shadow user names are *[user]-shadow*.

# Security

While I strive to keep my code straightforward and follow good security practices and prevent known vulnerabilities, I absolutely cannot guarantee its security as it wasn't audited by anyone else. This is a privileged program running with root privileges. Vulnerabilities may lead to privilege escalation and gaining root privileges. If you decide to use it, you do it at your own risk.

File permissions play important role in the overall security. Make sure all files in prefix are owned by `root:root` and don't have write permissions - same thing for global config file at `/etc/midsu` and sudoers configuration. Also make sure primary user cannot access shadow user's files. `.shadow-home` permissions should be 700. You should give the group execute bit to your primary user home directory to shadow user be able to enter its home directory.

```sh
# as primary user:
chmod g+x ~
chmod 700 ~/.shadow-home
```

If you also set read and write permissions on your primary homedir, shadow user will be able to see its content, add new files and delete existing. `chmid` is not going to function without that.

```sh
chmod g+rw ~
```

See also: [Protecting critical user configs](#protecting-critical-user-configs)

## (In)secure startup

Midsu, as well as other common GNU/Linux utilities, is vulnerable to attack of malicious user config. Attacker may gain access to your shadow user by constructing fake midsu script, overwrite `$PATH` environment variable and convince you to run `midsu` and enter your password. Then, it's possible to run arbitrary commands as shadow user by passing them via stdin to the real `midsu` at `/usr/local/bin/midsu`. They can also replace your terminal emulator or even window manager. Unfortunately I cannot do much about this, as this is how typical GNU/Linux system works.

What you can do to minimize the risk:

* ensure that you run correct executable or running `/usr/local/bin/midsu` instead,
* switch to virtual console (*ctrl+alt+f1-7*),
* think twice before you enter your password,
* use midlaunch + `run_as_shadow=true` + `permit_args=false` instead (see: [example midlaunch config](config/midlaunch))

## Protecting critical user configs

Files such as: `.bashrc`, `.bash_profile`, `.profile` or `.xsession` and `.xinitrc` are scripts that run at login time or when you open up terminal. Malicious programs can alter them to replace commands like `sudo`, `midsu` or `ssh`, to trick you into running them instead of the intended programs and read your keystrokes, terminal output or inject arbitrary code without you noticing it.

With midutils, you can make those files read-only for primary user, so you can modify them only as shadow user. That's a little bit tricky, because normally you are be able to delete files owned by other user, if they reside in your directory. It's because you have write permissions to a directory.

Sticky bit is an additional Linux permission that allows deleting only your files. Such bit is set on `/tmp` together with write permission to all, so anybody can create files there, but can delete only their own files.

Let's proceed step-by-step:

```sh
# set permissions of the desired file (no write for group)
chmod 644 ~/.bashrc

# change owner
chmid upgrade ~/.bashrc

# set sticky bit for your home directory
chmod +t ~

# change home owner for sticky bit to work
chmid upgrade ~
```

Next time you need to modify your `.bashrc`, you must log in as shadow first.

# Contributing

This project is Free Software, meaning everybody is allowed and encouraged to hack on it to adapt one's personal needs and to distribute modified versions under the terms of AGPLv3 license. Contributions such as: code reviews, security audits, bug fixes and improvements would be appreciated, as well as discussions and ideas.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
