import argparse
import os
import sys

from midsu import *


def chown(file: str, current_uid: str, target_uid: str, recursive: bool = False):
    file_info = os.stat(file, follow_symlinks=False)
    source_uid, source_gid = file_info.st_uid, file_info.st_gid
    
    # Exit if fs.protected_hardlinks is disabled
    with open('/proc/sys/fs/protected_hardlinks') as fd:
        protected_hardlinks = int(fd.read())
    if not protected_hardlinks:
        raise PermissionError("sysctl fs.protected_hardlinks is disabled. Chmid is vulnerable to time-of-check to time-of-use race condition.")
    
    # Stop if file is owned by someone else
    if source_uid == current_uid:
        os.chown(file, target_uid, source_gid, follow_symlinks=False)
    else:
        print(f"Warning: file '{file}' is owned by someone else")
    
    if recursive and os.path.isdir(file) and not os.path.islink(file):
        for subfile in os.scandir(file):
            chown(subfile.path, current_uid, target_uid, recursive)

def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(
        description='Move file to/from your shadow user'
    )
    parser.add_argument(
        '-R', '--recursive',
        action='store_true',
        help='Operate on directories recursively'
    )
    parser.add_argument(
        'mode',
        choices=('upgrade', 'downgrade'),
        help='Upgrade - set owner to shadow user, downgrade - conversly'
    )
    parser.add_argument(
        'files',
        nargs='+'
    )
    args = parser.parse_args()
    
    # Read config
    config = read_config(CONFIG_FILE)
    
    # Get current, primary and shadow username
    current_user = get_current_user()
    primary_user, shadow_user = get_current_mapping(current_user, dict(config['shadows']))
    
    # Exit if user is unmapped
    if not shadow_user:
        print("You don't have shadow user to hide files in")
        exit(1)
    
    # Determine the target user
    if args.mode == 'upgrade':
        target_user = shadow_user
    elif args.mode == 'downgrade':
        target_user = primary_user
    
    # Exit if target user does not exist
    if not user_exists(target_user):
        raise ValueError(f"User '{target_user}' does not exist")
    
    current_uid = pwd.getpwnam(current_user).pw_uid
    target_uid = pwd.getpwnam(target_user).pw_uid
    
    # Exit if targeting self
    if current_user == target_user:
        print("Use appropriate user for this operation.")
        print("Upgrading means moving ownership to shadow user. Only primary user can do that.")
        print("Downgrading means moving ownership to primary user. Only shadow user can do that.")
        exit(1)
    
    restart_with_sudo(__file__)
    
    for name in args.files:
        # Skip non-existing files
        if not os.path.exists(name):
            print(f"Warning: file '{name}' does not exist")
            continue
        
        # Change file owner
        chown(name, current_uid, target_uid, recursive=args.recursive)


if __name__ == '__main__':
    main()
