bin = /usr/local/bin
prefix = /usr/local/lib/midsu

utils = midsu chmid midlaunch
sudoers = /etc/sudoers.d/99-midsu

all: install

install:
	mkdir -p $(prefix)
	
	for i in $(utils); do \
		cp $$i.py $(prefix); \
		ln -sf $(prefix)/launcher.py $(bin)/$$i; \
	done
	
	sed "s,%PREFIX%,$(prefix),g" < launcher.py > $(prefix)/launcher.py
	sed "s,%PREFIX%,$(prefix),g" < sudoers > $(sudoers)
	chmod +x $(prefix)/launcher.py
	
	[ ! -e /etc/midsu ] && cp config/midsu /etc/midsu || true

clean:
	rm -Rf $(prefix) $(sudoers)
	for i in $(utils); do \
		rm $(bin)/$$i; \
	done
