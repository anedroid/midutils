import argparse
import configparser
import os
import pwd
from pprint import pprint

from midsu import *


APP_CONFIG_FILE = '.midlaunch'


def read_app_config(config_file: str, homedir: (str, str)) -> dict:
    """Loads app config file, validates it and returns processed form and
    exec-section profile
    """
    
    config = configparser.ConfigParser(default_section=None)
    primary_homedir, shadow_homedir = homedir
    
    # Load config file
    if not os.path.isfile(config_file):
        raise FileNotFoundError(f"Global config file ({config_file}) not found")
    if not os.access(config_file, os.R_OK, effective_ids=True):
        raise PermissionError(f"Access denied for app config file ({config_file}")
    config.read(config_file)
    
    # Convert to dict
    config_dict = {}
    exec_to_section = {}
    for section in config.sections():
        obj = config_dict[section] = dict(config[section])
        obj['run_as_shadow'] = config[section].getboolean('run_as_shadow', False)
        obj['permit_args'] = config[section].getboolean('permit_args', True)
        
        # Parse mounts
        if 'mounts' in obj:
            # mapping is target:source because of relation 1:n
            mounts = {}
            for mount in obj['mounts'].strip().split('\n'):
                source, target = parse_mount_string(mount.strip())
                
                # Expand paths
                if source and not source.startswith('/'):
                    source = os.path.join(shadow_homedir, source)
                if target and not target.startswith('/'):
                    target = os.path.join(primary_homedir, target)
                
                mounts[target] = source
            obj['mounts'] = mounts
        
        # Multiple exec fields
        if 'exec' in obj:
            execs = []
            for command in obj['exec'].strip().split('\n'):
                command = command_to_args(command.strip())
                execs.append(command)
                if command in exec_to_section:
                    raise ValueError(f"Duplicate exec found: {command}")
                exec_to_section[command] = section
            obj['exec'] = execs
    
    # Resolve inheritance
    resolved = resolve_inheritance(config_dict)
    
    # Find exec duplicates (after resolving)
    all_execs = []
    for obj in resolved.values():
        for command in obj.get('exec', []):
            if command in all_execs:
                raise ValueError(f"Duplicate exec found (inherited): {command}")
            all_execs.append(command)
    
    return resolved, exec_to_section

def command_to_args(command: str) -> tuple:
    """Convert command string to args tuple
    """
    
    parts = departialize(command)
    if not len(parts) % 2:
        raise ValueError(f"Invalid exec: unclosed quote")
    
    args = []
    current_arg = ''
    for i, part in enumerate(parts):
        if i % 2:
            current_arg += part
            continue
        for j, part in enumerate(part.split(' ')):
            if not part:
                if current_arg:
                    args.append(current_arg)
                    current_arg = ''
                continue
            if j and current_arg:
                args.append(current_arg)
                current_arg = ''
            current_arg += part
    
    if current_arg:
        args.append(current_arg)
    
    return tuple(args)

def parse_mount_string(mount: str) -> (str, str):
    """Extract source and target from mount string
    """
    
    def error(msg: str):
        raise ValueError(f"Invalid mount ({mount}): {msg}")
    
    parts = departialize(mount)
    if not len(parts) % 2:
        error("unclosed quote")
    
    empty_counter = 0
    source = target = None
    arrow_seen = False
    
    for i, part in enumerate(parts):
        if not part:
            empty_counter += 1
            if empty_counter > 1:
                error("empty value")
            continue
        else:
            empty_counter = 0
        
        if quoted := bool(i % 2):
            if not source:
                source = part
            elif not arrow_seen:
                error("multiple sources") # a"a"->b
            elif not target:
                target = part
            else:
                error("multiple targets") # a->b"b"
        elif '->' in part:
            part_split = part.split('->')
            if arrow_seen or len(part_split) > 2:
                error("multiple arrows") # a->b->c
            arrow_seen = True
            left = part_split[0].strip()
            right = part_split[1].strip()
            
            if not source:
                if left:
                    source = left
                else:
                    # commented out to permit empty source (meaning deleting mount)
                    #error("empty source") # ->b
                    pass
            elif source and left:
                error("multiple sources") # "a"a->b
            
            if right:
                target = right
        elif not arrow_seen:
            if source:
                error("multiple sources") # "a"a
            source = part
        else:
            error("multiple targets") # a->"b"b
    
    if not source and not target:
        error("empty value")
    if not target:
        if arrow_seen:
            error("empty target") # a->
        target = source
    
    return source, target

def resolve_inheritance(input_dict: dict) -> dict:
    """Calculate properties for config objects based on "extends" property
    """
    
    resolved = {}
    in_progress = set()
    
    def resolve_properties(name: str) -> dict:
        if name in resolved:
            return resolved[name]
        
        if name in in_progress:
            raise RecursionError(f"Circular dependency of objects: {in_progress}")
        
        if name not in input_dict:
            raise KeyError(f"Object '{name}' not defined")
        
        in_progress.add(name)
        obj = input_dict[name]
        
        if 'extends' in obj:
            # extract dicts from obj to merge them later
            obj_dicts = {}
            for subname in tuple(obj.keys()):
                if type(obj[subname]) == dict:
                    obj_dicts[subname] = obj.pop(subname)
            
            # merge non-dict properties
            obj = {**resolve_properties(obj['extends']), **obj}
            
            # merge extracted dicts
            for subname in obj_dicts:
                original = obj[subname] if subname in obj else {}
                new = obj_dicts[subname]
                obj[subname] = {**original, **new}
        
        resolved[name] = obj
        in_progress.remove(name)
        return obj
    
    for name in input_dict:
        resolve_properties(name)
    
    return resolved

def departialize(input_str: str, quotes: str = '"\'') -> list:
    """Divide string into "quoted" and unquoted parts.
    Quoted parts can escape characters (\)
    """
    
    parts = []
    current_part = ''
    quoted = False
    escaping = False
    end_char = None
    
    for char in input_str:
        if quoted:
            if escaping:
                current_part += char
                escaping = False
            elif char == '\\':
                escaping = True
            elif char == end_char:
                parts.append(current_part)
                current_part = ''
                quoted = False
            else:
                current_part += char
        else:
            if char in quotes:
                parts.append(current_part)
                current_part = ''
                quoted = True
                end_char = char
            else:
                current_part += char
        
    parts.append(current_part)
    return parts

def best_match(item_list: list, test: tuple) -> tuple:
    """Finds the longest matching item from the list which is the beginning of the value
    """
    result = None
    
    for item in item_list:
        if item == test[:len(item)]:
            if result == None or len(item) > len(result):
                result = item
    
    return result

def launch_program(argv: list, config: dict, user: str, verbose: bool = False):
    """Run program as user with given configuration.
    This function assumes, that all the validation has been already done.
    """
    cmd_args = [
        'bwrap',
        '--dev-bind', '/', '/',
    ]
    for target, source in config.get('mounts', {}).items():
        cmd_args += ['--bind', source, target]
    cmd_args += [
        '--',
        PATH['runuser'],
        '-u', user,
        '--',
        *argv
    ]
    
    if verbose:
        print(' '.join(cmd_args))
    
    # Here comes the final call:
    os.execv(PATH['bwrap'], cmd_args)

def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(
        description='Run **trusted** programs with exclusive filesystem access'
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help='display pre-startup debug messages'
    )
    parser.add_argument(
        'program',
        help='absolute path to executable'
    )
    parser.add_argument(
        'args',
        nargs='...',
        help='arguments passed to executing program'
    )
    args = parser.parse_args()
    
    # Validate executable path
    if not args.program.startswith('/'):
        print("Absolute path is expected")
        exit(1)
    
    is_executable = os.path.isfile(args.program) and \
        os.access(args.program, os.R_OK|os.X_OK)
    if not is_executable:
        print("Executable not found")
        exit(1)
    
    # Read (system) config
    config = read_config(CONFIG_FILE)
    
    # Get current, primary and shadow username
    current_user = get_current_user()
    primary_user, shadow_user = get_current_mapping(current_user,
        dict(config['shadows']))
    
    # Exit if user is unmapped
    if not shadow_user:
        print("You don't have shadow user storing your secrets")
        exit(1)
    
    # Set process privileges
    restart_with_sudo(__file__)
    run_as(shadow_user)
    # << RUNNING AS SHADOW USER >>
    
    primary_homedir = pwd.getpwnam(primary_user).pw_dir
    shadow_homedir = pwd.getpwnam(shadow_user).pw_dir
    
    # Read app config
    app_config_file = os.path.join(shadow_homedir, APP_CONFIG_FILE)
    app_config, exec_to_section = read_app_config(
        app_config_file,
        homedir=(primary_homedir, shadow_homedir)
    )
    
    # Exit if program is not configured
    app_argv = (args.program, *args.args)
    match = best_match(exec_to_section.keys(), app_argv)
    if not match:
        print(f"Exec {args.program} is not specified in .midlaunch config.")
        exit(1)
    running_app_config = app_config[exec_to_section[match]]
    
    # Display processed startup configuration
    if args.verbose:
        print(exec_to_section[match])
        pprint(running_app_config)
    
    # Exit if violated args policy
    if not running_app_config['permit_args'] and app_argv != match:
        print(f"Additional arguments are prohibited for this program")
        exit(1)
    
    if mounts := running_app_config.get('mounts'):
        for target, source in tuple(mounts.items()):
            # Delete null mounts
            if not source:
                del mounts[target]
                continue
            
            # Check permissions of source
            source_safe = os.path.isdir(source) and \
                (os.path.realpath(source).startswith(primary_homedir) or \
                 os.path.realpath(source).startswith(shadow_homedir)) and \
                os.access(source, os.R_OK|os.X_OK, effective_ids=True)
            if not source_safe:
                raise PermissionError(f"Access denied to {source}")
        
        # Check permissions of target
        run_as(primary_user)
        # << RUNNING AS PRIMARY USER >>
        for target in mounts.keys():
            target_safe = os.path.isdir(target) and \
                os.path.realpath(target).startswith(primary_homedir) and not \
                os.path.realpath(target).startswith(shadow_homedir) and \
                os.access(target, os.R_OK|os.X_OK, effective_ids=True)
            if not target_safe:
                raise PermissionError(f"Access denied to {target}")
    
    run_as()
    # << RUNNING AS ROOT >>
    t_user = shadow_user if running_app_config['run_as_shadow'] else primary_user
    launch_program(
        app_argv,
        running_app_config,
        user=t_user,
        verbose=args.verbose
    )


if __name__ == '__main__':
    main()
